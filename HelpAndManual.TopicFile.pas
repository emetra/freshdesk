unit HelpAndManual.TopicFile;

interface

uses
  HelpAndManual,
  HelpAndManual.Keywords,
  System.Generics.Collections,
  Xml.XmlIntf;

type
  THmTopicFile = class( TObject )
  strict private
    fTopic: THmTopic;
    fXmlDoc: IXmlDocument;
    fKeywords: TList<TKeyword>;
  public
    { Intialization }
    constructor CreateFromFile( const AFileName: string );
    procedure SaveAsHtml( const AIncludeBootstrap: boolean );
    procedure BeforeDestruction; override;
    { Properties }
    property Topic: THmTopic read fTopic;
  end;

implementation

uses
  Html5Template,
  System.SysUtils, Xml.XmlDoc;

{ THmTopicFile }

constructor THmTopicFile.CreateFromFile( const AFileName: string );
var
  c: IXmlNode;
  keywordsNode: IXmlNode;
  n: integer;
begin
  inherited Create;
  Assert( FileExists( AFileName ), 'File missing' );
  fKeywords := TList<TKeyword>.Create;
  fXmlDoc := LoadXmlDocument( AFileName );
  Assert( Assigned( fXmlDoc.DocumentElement ), 'Found no document element' );
  fTopic := THmTopic.Create( EmptyStr, fXmlDoc.DocumentElement );
  { Get keywords here, as they don't really map to any html element and }
  keywordsNode := fXmlDoc.DocumentElement.ChildNodes.FindNode( 'keywords' );
  if Assigned( keywordsNode ) then
  begin
    n := 0;
    while n < keywordsNode.ChildNodes.Count do
    begin
      c := keywordsNode.ChildNodes[n];
      if ( c.IsTextElement ) and ( c.Text <> EmptyStr ) then
        fKeywords.Add( TKeyword.Create( c ) );
      inc( n );
    end;
  end;
end;

procedure THmTopicFile.BeforeDestruction;
begin
  fKeywords.Free;
  fTopic.Free;
  inherited;
end;

procedure THmTopicFile.SaveAsHtml( const AIncludeBootstrap: boolean );
var
  htmlFile: THtml5File;
begin
  htmlFile := THtml5File.Create( fTopic.Title, fTopic.Body, AIncludeBootstrap );
  try
    htmlFile.SaveToFile( ChangeFileExt( fXmlDoc.FileName, '.html' ) );
  finally
    htmlFile.Free;
  end;
end;

end.
