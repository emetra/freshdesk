unit HelpToFresh;

interface

uses
  HelpAndManual,
  HelpAndManual.TopicFile,
  Freshdesk.Article;

type
  THelpConverter = class( TObject )
    procedure Copy( const AFileName: string; out AFreshDeskArticle: TFreshdeskArticle );
  end;

implementation

{ THelpConverter }

procedure THelpConverter.Copy( const AFileName: string; out AFreshDeskArticle: TFreshdeskArticle );
var
  hm: THmTopicFile;
begin
  hm := THmTopicFile.CreateFromFile( AFileName );
  AFreshDeskArticle := TFreshdeskArticle.Create;
  AFreshDeskArticle.UpdateBody( hm.Topic.Body );
  AFreshDeskArticle.Title := ( hm.Topic.Title );
  { Not yet implemented }
  // AFreshdeskArticle.CreatedAt := hm.Topic.CreatedAt;
  // AFreshdeskArticle.UpdatedAt := hm.Topic.UpdatedAt;
end;

end.
