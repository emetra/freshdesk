program FreshdeskTest;

uses
  Emetra.Logging.SmartInspect,
  Vcl.Forms,
  MainFreshdeskTest in 'MainFreshdeskTest.pas' {frmMainFreshdeskViewer},
  Emetra.VclUtil.ListBoxPainter in '..\..\FastTrak\LIB\GUI\Emetra.VclUtil.ListBoxPainter.pas',
  Emetra.VclFrame.Browser in '..\..\FastTrak\LIB\GUI\Emetra.VclFrame.Browser.pas' {frmBrowser: TFrame},
  HelpAndManual in '..\HelpAndManual.pas',
  Html5Template in '..\Html5Template.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := true;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMainFreshdeskViewer, frmMainFreshdeskViewer);
  Application.Run;
end.
