unit MainFreshdeskTest;

interface

uses
  Freshdesk.Article,
  Emetra.VclFrame.Browser,
  Emetra.VclUtil.ListBoxPainter,
  Emetra.Logging.Interfaces,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, REST.Types, REST.Client, REST.Authenticator.Basic, Data.Bind.Components, Data.Bind.ObjectScope, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, SynEdit, SynEditHighlighter, SynHighlighterHtml;

type
  /// <summary>
  /// This form allows the user to view the hierarchy of articles in Freshdesh.
  /// Documentation for the API can be found here: https://developer.freshdesk.com/api/
  /// </summary>
  TfrmMainFreshdeskViewer = class( TForm )
    restFreshdeskClient: TRESTClient;
    restCategoriesRequest: TRESTRequest;
    restFreshdeskAuthenticator: THTTPBasicAuthenticator;
    RESTResponse1: TRESTResponse;
    restFoldersRequest: TRESTRequest;
    RESTResponse2: TRESTResponse;
    restArticlesRequest: TRESTRequest;
    RESTResponse3: TRESTResponse;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    lbCategories: TListBox;
    lbFolders: TListBox;
    lbArticles: TListBox;
    PageControl1: TPageControl;
    tbSource: TTabSheet;
    tbsHtml: TTabSheet;
    SynEdit1: TSynEdit;
    SynHTMLSyn1: TSynHTMLSyn;
    procedure FormCreate( Sender: TObject );
    procedure FormDestroy( Sender: TObject );
    procedure BitBtn1Click( Sender: TObject );
    procedure lbCategoriesClick( Sender: TObject );
    procedure lbFoldersClick( Sender: TObject );
    procedure lbArticlesClick( Sender: TObject );
  private
    { Private declarations }
    fCategoryResponse: TFreshdeskCategoriesResponse;
    fFolderResponse: TFreshdeskFoldersResponse;
    fArticleResponse: TFreshdeskArticleResponse;
    fPainter: TCustomListControlPainter;
    fBrowser: TfrmBrowser;
    function DoArticleCleanup( const AFreshdeskArticle: TFreshdeskArticle ): string;
  public
    { Public declarations }
  end;

var
  frmMainFreshdeskViewer: TfrmMainFreshdeskViewer;

implementation

uses
  Html5Template,
  REST.JSON, Clipbrd, System.RegularExpressions;

{$R *.dfm}

procedure TfrmMainFreshdeskViewer.FormCreate( Sender: TObject );
begin
  fPainter := TCustomListControlPainter.Create( lbCategories );
  fPainter.Attach( lbCategories );
  fPainter.Attach( lbFolders );
  fPainter.Attach( lbArticles );
  fBrowser := TfrmBrowser.Create( Self, GlobalLog );
  fBrowser.Prepare( tbsHtml, alClient );
  { Copied properties from the form }
  with restFreshdeskClient do
  begin
    Authenticator := restFreshdeskAuthenticator;
    Accept := 'application/json';
    AcceptCharset := 'utf-8, *;q=0.8';
    BaseURL := 'https://ulriken.freshdesk.com/api/v2';
  end;
  with restFreshdeskAuthenticator do
  begin
    Username := 'QZkOTxT6dj8pC35yZ2o';
    Password := 'X';
  end;
  with restCategoriesRequest do
  begin
    Client := restFreshdeskClient;
    Resource := 'solutions/categories';
    Response := RESTResponse1;
  end;
  with restFoldersRequest do
  begin
    Client := restFreshdeskClient;
    Resource := 'solutions/categories/{categoryId}/folders';
    Response := RESTResponse2
  end;
  with restArticlesRequest do
  begin
    Client := restFreshdeskClient;
    Resource := 'solutions/folders/{folderId}/articles';
    Response := RESTResponse3;
  end;
end;

procedure TfrmMainFreshdeskViewer.FormDestroy( Sender: TObject );
begin
  fPainter.Detach( lbArticles );
  fPainter.Detach( lbFolders );
  fPainter.Detach( lbCategories );
  fArticleResponse.Free;
  fFolderResponse.Free;
  fCategoryResponse.Free;
end;

procedure TfrmMainFreshdeskViewer.BitBtn1Click( Sender: TObject );
var
  c: TFreshdeskCategory;
begin
  lbCategories.Clear;
  restCategoriesRequest.Execute;
  BitBtn1.Caption := Format( 'Result %d - %s', [RESTResponse1.StatusCode, RESTResponse1.StatusText] );
  try
    fCategoryResponse := TJson.JsonToObject<TFreshdeskCategoriesResponse>( '{ "categories" : ' + RESTResponse1.Content + '}' );
    for c in fCategoryResponse.Categories do
      lbCategories.Items.AddObject( c.Name, c );
  except
    on E: Exception do

  end;
  Clipboard.AsText := RESTResponse1.Content;
end;

procedure TfrmMainFreshdeskViewer.lbCategoriesClick( Sender: TObject );
var
  c: TFreshdeskCategory;
  f: TFreshdeskFolder;
begin
  lbArticles.Clear;
  lbFolders.Clear;
  FreeAndNil( fFolderResponse );
  FreeAndNil( fArticleResponse );
  if lbCategories.ItemIndex <> -1 then
  begin
    c := lbCategories.Items.Objects[lbCategories.ItemIndex] as TFreshdeskCategory;
    try
      restFoldersRequest.Params[0].Value := IntToStr( c.Id );
      restFoldersRequest.Execute;
      fFolderResponse := TJson.JsonToObject<TFreshdeskFoldersResponse>( '{ "folders" : ' + RESTResponse2.Content + '}' );
      for f in fFolderResponse.Folders do
        lbFolders.Items.AddObject( f.Name, f );
    except
      on E: Exception do
        lbFolders.Items.Add( E.Message );
    end;
  end;
end;

procedure TfrmMainFreshdeskViewer.lbFoldersClick( Sender: TObject );
var
  f: TFreshdeskFolder;
  a: TFreshdeskArticle;
begin
  lbArticles.Clear;
  FreeAndNil( fArticleResponse );
  if lbFolders.ItemIndex <> -1 then
  begin
    f := lbFolders.Items.Objects[lbFolders.ItemIndex] as TFreshdeskFolder;
    try
      restArticlesRequest.Params[0].Value := IntToStr( f.Id );
      restArticlesRequest.Execute;
      fArticleResponse := TJson.JsonToObject<TFreshdeskArticleResponse>( '{ "articles" : ' + RESTResponse3.Content + '}' );
      for a in fArticleResponse.Articles do
        lbArticles.Items.AddObject( a.Name, a );
    except
      on E: Exception do
        lbArticles.Items.Add( E.Message )
    end;
  end;
end;

procedure TfrmMainFreshdeskViewer.lbArticlesClick( Sender: TObject );
var
  freshdeskArticle: TFreshdeskArticle;
  htmlFile: THtml5File;
begin
  if lbArticles.ItemIndex <> -1 then
  begin
    freshdeskArticle := lbArticles.Items.Objects[lbArticles.ItemIndex] as TFreshdeskArticle;
    htmlFile := THtml5File.Create( freshdeskArticle.Title, DoArticleCleanup( freshdeskArticle ), true );
    try
      fBrowser.Navigate( htmlFile );
      SynEdit1.Text := htmlFile.Text;
      htmlFile.SaveToFile( Format( 'article-%d.html', [freshdeskArticle.Id] ) );
    finally
      htmlFile.Free;
    end;
  end;
end;

function TfrmMainFreshdeskViewer.DoArticleCleanup( const AFreshdeskArticle: TFreshdeskArticle ): string;
begin
  Result := AFreshdeskArticle.Description;
  Result := TRegEx.Replace( Result, '</?span[^>]*>', EmptyStr );
  Result := TRegEx.Replace( Result, '<p>\s*</p>', EmptyStr );
  Result := TRegEx.Replace( Result, 'style="[^"]*"', EmptyStr );
  { Headers }
  Result := StringReplace( Result, '<p><strong>', '<h3>', [rfReplaceAll] );
  Result := TRegEx.Replace( Result, '</strong>[\s:]*</p>', '</h3>' );
  { Excessive paragraphing }
  Result := StringReplace( Result, '<p><br></p>', EmptyStr, [rfReplaceAll] );
end;

end.
