object frmMainFreshdeskViewer: TfrmMainFreshdeskViewer
  Left = 0
  Top = 0
  Caption = 'frmMainFreshdeskViewer'
  ClientHeight = 811
  ClientWidth = 1209
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 15
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 337
    Height = 811
    Align = alLeft
    Caption = 'Panel2'
    TabOrder = 0
    object BitBtn1: TBitBtn
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 329
      Height = 25
      Align = alTop
      Caption = 'Get categories'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object lbCategories: TListBox
      AlignWithMargins = True
      Left = 4
      Top = 35
      Width = 329
      Height = 185
      Align = alTop
      ItemHeight = 15
      TabOrder = 1
      OnClick = lbCategoriesClick
    end
    object lbFolders: TListBox
      AlignWithMargins = True
      Left = 4
      Top = 226
      Width = 329
      Height = 178
      Align = alTop
      ItemHeight = 15
      TabOrder = 2
      OnClick = lbFoldersClick
    end
    object lbArticles: TListBox
      AlignWithMargins = True
      Left = 4
      Top = 410
      Width = 329
      Height = 397
      Align = alClient
      ItemHeight = 15
      TabOrder = 3
      OnClick = lbArticlesClick
    end
  end
  object PageControl1: TPageControl
    Left = 337
    Top = 0
    Width = 872
    Height = 811
    ActivePage = tbSource
    Align = alClient
    TabOrder = 1
    object tbSource: TTabSheet
      Caption = 'Source'
      ImageIndex = 1
      object SynEdit1: TSynEdit
        Left = 0
        Top = 0
        Width = 864
        Height = 781
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Consolas'
        Font.Style = []
        Font.Quality = fqClearTypeNatural
        TabOrder = 0
        UseCodeFolding = False
        Gutter.Font.Charset = DEFAULT_CHARSET
        Gutter.Font.Color = clWindowText
        Gutter.Font.Height = -11
        Gutter.Font.Name = 'Consolas'
        Gutter.Font.Style = []
        Highlighter = SynHTMLSyn1
        Lines.Strings = (
          'SynEdit1')
      end
    end
    object tbsHtml: TTabSheet
      Caption = 'HTML'
    end
  end
  object restFreshdeskClient: TRESTClient
    Authenticator = restFreshdeskAuthenticator
    Accept = 'application/json'
    AcceptCharset = 'utf-8, *;q=0.8'
    AutoCreateParams = False
    BaseURL = 'https://ulriken.freshdesk.com/api/v2'
    Params = <>
    Left = 464
    Top = 24
  end
  object restCategoriesRequest: TRESTRequest
    Client = restFreshdeskClient
    Params = <>
    Resource = 'solutions/categories'
    Response = RESTResponse1
    SynchronizedEvents = False
    Left = 392
    Top = 89
  end
  object restFreshdeskAuthenticator: THTTPBasicAuthenticator
    Username = 'QZkOTxT6dj8pC35yZ2o'
    Password = 'X'
    Left = 288
    Top = 16
  end
  object RESTResponse1: TRESTResponse
    Left = 480
    Top = 88
  end
  object restFoldersRequest: TRESTRequest
    AssignedValues = [rvAccept]
    Accept = 'application/json'
    Client = restFreshdeskClient
    Params = <
      item
        Kind = pkURLSEGMENT
        Name = 'categoryId'
        Options = [poAutoCreated]
      end>
    Resource = 'solutions/categories/{categoryId}/folders'
    Response = RESTResponse2
    SynchronizedEvents = False
    Left = 392
    Top = 160
  end
  object RESTResponse2: TRESTResponse
    Left = 472
    Top = 160
  end
  object restArticlesRequest: TRESTRequest
    AssignedValues = [rvAccept]
    Accept = 'application/json'
    Client = restFreshdeskClient
    Params = <
      item
        Kind = pkURLSEGMENT
        Name = 'folderId'
        Options = [poAutoCreated]
      end>
    Resource = 'solutions/folders/{folderId}/articles'
    Response = RESTResponse3
    SynchronizedEvents = False
    Left = 392
    Top = 232
  end
  object RESTResponse3: TRESTResponse
    Left = 472
    Top = 232
  end
  object SynHTMLSyn1: TSynHTMLSyn
    Options.AutoDetectEnabled = False
    Options.AutoDetectLineLimit = 0
    Options.Visible = False
    Left = 581
    Top = 330
  end
end
