program TestClassHelpAndManual;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  Emetra.Logging.SmartInspect,
  DUnitTestRunner,
  HelpAndManual in '..\HelpAndManual.pas',
  TestCaseHelpAndManual in 'TestCaseHelpAndManual.pas',
  Html5Template in '..\Html5Template.pas',
  HelpToFresh in '..\HelpToFresh.pas',
  Emetra.Interfaces.Listbox in '..\..\FastTrak\LIB\Service\Emetra.Interfaces.Listbox.pas',
  Freshdesk.Article in '..\Freshdesk.Article.pas',
  Freshdesk.Api in '..\Freshdesk.Api.pas',
  HelpAndManual.TopicFile in '..\HelpAndManual.TopicFile.pas',
  HelpAndManual.Keywords in '..\HelpAndManual.Keywords.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

