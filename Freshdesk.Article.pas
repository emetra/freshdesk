unit Freshdesk.Article;

interface

uses
  Emetra.Interfaces.ListBox,
  System.Generics.Collections, System.Classes, REST.Json.Types;

{$M+}

type
  {
    TCloudFiles = class
    end;

    TAttachments = class
    end;

    TTags = class
    end;

    TSeoData = class
    private
    [JSONName( 'meta_description' )]
    FMetaDescription: string;
    [JSONName( 'meta_title' )]
    FMetaTitle: string;
    published
    property MetaDescription: string read FMetaDescription write FMetaDescription;
    property MetaTitle: string read FMetaTitle write FMetaTitle;
    end;
  }

  TFreshdeskBaseClass = class( TInterfacedPersistent )
  private
    FId: Int64;
    FName: string;
    [JSONName( 'created_at' )]
    FCreatedAt: TDateTime;
    [JSONName( 'updated_at' )]
    FUpdatedAt: TDateTime;
    FDescription: string;
  protected
    function V: string; { Code value, painted in blue usually }
    function DN: string; dynamic; { Code name, painted in black }
    function OT: string; { Other text, painted in red }
    function Match( const ASearchText: string ): boolean;
    function IsCurrent: boolean;
    function AsListBox( const ASimple: boolean = true ): string;
  public
    function Description: string;
  published
    property CreatedAt: TDateTime read FCreatedAt write FCreatedAt;
    property Id: Int64 read FId write FId;
    property UpdatedAt: TDateTime read FUpdatedAt write FUpdatedAt;
    property name: string read FName write FName;
  end;

  TFreshdeskFolder = class( TFreshdeskBaseClass, IListBoxBase )
  strict private
    FVisibility: Integer;
  published
    property Visibility: Integer read FVisibility write FVisibility;
  end;

  TFreshDeskFolders = array of TFreshdeskFolder;

  TFreshdeskCategory = class( TFreshdeskBaseClass, IListBoxBase, IListBoxItem )
  strict private
  end;

  TFreshDeskCategories = array of TFreshdeskCategory;

  TFreshDeskCategoriesResponse = class( TObject )
  strict private
    FCategories: TFreshDeskCategories;
  public
    procedure BeforeDestruction; override;
    { Properties }
    property Categories: TFreshDeskCategories read FCategories;
  end;

  TFreshDeskFoldersResponse = class( TObject )
  strict private
    FFolders: TFreshDeskFolders;
  public
    procedure BeforeDestruction; override;
    { Properties }
    property Folders: TFreshDeskFolders read FFolders;
  end;

  TFreshDeskArticlePost = class( TObject )
  strict private
    fTitle: string;
    FDescription: string;
    fStatus: Integer;
  public
    constructor Create( const ATitle, ADescription: string ); reintroduce;
  end;

  TFreshdeskArticle = class( TFreshdeskBaseClass, IListBoxBase )
  strict private
    // fSEOData: TSeoData;
    // [JSONName( 'attachments' ), JSONMarshalled( False )]
    // FAttachmentsArray: TArray<TAttachments>;
  [JSONName( 'agent_id' )]
    FAgentId: Int64;
    // [GenericListReflect]
    // Attachments: TObjectList<TAttachments>;
    [JSONName( 'category_id' )]
    FCategoryId: Int64;
    // [JSONName( 'cloud_files' ), JSONMarshalled( False )]
    // FCloudFilesArray: TArray<TCloudFiles>;
    // [GenericListReflect]
    // FCloudFiles: TObjectList<TCloudFiles>;
    [JSONName( 'description_text' )]
    FDescriptionText: string;
    [JSONName( 'feedback_count' )]
    FFeedbackCount: Integer;
    [JSONName( 'folder_id' )]
    FFolderId: Int64;
    FHits: Integer;
    [JSONName( 'seo_data' )]
    fStatus: Integer;
    // [JSONName( 'tags' ), JSONMarshalled( False )]
    // FTagsArray: TArray<TTags>;
    // [GenericListReflect]
    // FTags: TObjectList<TTags>;
    [JSONName( 'thumbs_down' )]
    FThumbsDown: Integer;
    [JSONName( 'thumbs_up' )]
    FThumbsUp: Integer;
    fTitle: string;
    FType: Integer;
    { Interfaces }
  protected
    function DN: string; override;
  protected
    Description: string;
  public
    { Initialization }
    constructor Create; reintroduce; dynamic;
    destructor Destroy; override;
    { }
    procedure UpdateBody( const ABody: string );
    function GetAsJson: string; dynamic;
    // function GetAttachments: TObjectList<TAttachments>;
    // function GetCloudFiles: TObjectList<TCloudFiles>;
    // function GetTags: TObjectList<TTags>;
  published
    // property Attachments: TObjectList<TAttachments> read GetAttachments;
    property AgentId: Int64 read FAgentId write FAgentId;
    property CategoryId: Int64 read FCategoryId write FCategoryId;
    // property CloudFiles: TObjectList<TCloudFiles> read GetCloudFiles;
    property DescriptionText: string read FDescriptionText write FDescriptionText;
    property FeedbackCount: Integer read FFeedbackCount write FFeedbackCount;
    property FolderId: Int64 read FFolderId write FFolderId;
    property Hits: Integer read FHits write FHits;
    // property SeoData: TSeoData read fSEOData;
    property Status: Integer read fStatus write fStatus;
    // property Tags: TObjectList<TTags> read GetTags;
    property ThumbsDown: Integer read FThumbsDown write FThumbsDown;
    property ThumbsUp: Integer read FThumbsUp write FThumbsUp;
    property Title: string read fTitle write fTitle;
    property &Type: Integer read FType write FType;
  end;

  TFreshdeskArticles = array of TFreshdeskArticle;

  TFreshDeskArticleResponse = class( TObject )
  strict private
    FArticles: TFreshdeskArticles;
  public
    procedure BeforeDestruction; override;
    property Articles: TFreshdeskArticles read FArticles;
  end;

implementation

uses
  System.SysUtils, System.RegularExpressions;

{ TJsonDto }

function TFreshdeskBaseClass.DN: string;
begin
  Result := FName;
end;

function TFreshdeskBaseClass.IsCurrent: boolean;
begin
  Result := true;
end;

function TFreshdeskBaseClass.Match( const ASearchText: string ): boolean;
begin
  Result := TRegEx.IsMatch( ASearchText, AsListBox );
end;

function TFreshdeskBaseClass.OT: string;
begin
  Result := DateToStr( FCreatedAt );
end;

function TFreshdeskBaseClass.V: string;
begin
  Result := IntToStr( FId );
end;

function TFreshdeskBaseClass.Description: string;
begin
  Result := FDescription;
end;

{ TRoot }

constructor TFreshdeskArticle.Create;
begin
  inherited;
  // fSEOData := TSeoData.Create;
end;

destructor TFreshdeskArticle.Destroy;
begin
  // fSEOData.Free;
  // GetTags.Free;
  // GetAttachments.Free;
  // GetCloudFiles.Free;
  inherited;
end;

function TFreshdeskArticle.DN: string;
begin
  Result := fTitle;
end;

// function TFreshdeskArticle.GetAttachments: TObjectList<TAttachments>;
// begin
// Result := TObjectList<TAttachments>(FAttachments, FAttachmentsArray);
// end;

// function TFreshdeskArticle.GetCloudFiles: TObjectList<TCloudFiles>;
// begin
// Result := TObjectList<TCloudFiles>(FCloudFiles, FCloudFilesArray);
// end;

// function TFreshdeskArticle.GetTags: TObjectList<TTags>;
// begin
// Result := TObjectList<TTags>(FTags, FTagsArray);
// end;

function TFreshdeskArticle.GetAsJson: string;
begin
  {
    RefreshArray<TAttachments>(FAttachments, FAttachmentsArray);
    RefreshArray<TCloudFiles>(FCloudFiles, FCloudFilesArray);
    RefreshArray<TTags>(FTags, FTagsArray);
    Result := inherited;
  }
end;

procedure TFreshdeskArticle.UpdateBody( const ABody: string );
begin
  FDescription := ABody;
end;

{ TFreshDeskCategoriesResponse }

procedure TFreshDeskCategoriesResponse.BeforeDestruction;
var
  c: TFreshdeskCategory;
begin
  for c in FCategories do
    c.Free;
  inherited;
end;

{ TFreshdeskCategory }

function TFreshdeskBaseClass.AsListBox( const ASimple: boolean ): string;
begin
  if ASimple then
    Result := V + #9 + DN + #9#9 + OT
  else
    Result := V + #9 + DN + #9 + FDescription + #9 + OT;
end;

{ TFreshDeskFoldersResponse }

procedure TFreshDeskFoldersResponse.BeforeDestruction;
var
  f: TFreshdeskFolder;
begin
  for f in FFolders do
    f.Free;
  inherited;
end;

{ TFreshDeskArticleResponse }

procedure TFreshDeskArticleResponse.BeforeDestruction;
var
  a: TFreshdeskArticle;
begin
  for a in FArticles do
    a.Free;
  inherited;
end;

{ TFreshDeskArticlePost }

constructor TFreshDeskArticlePost.Create( const ATitle, ADescription: string );
begin
  inherited Create;
  if ATitle = EmptyStr then
    fTitle := '(untitled)'
  else
    fTitle := ATitle;
  if ADescription = EmptyStr then
    FDescription := '(no description)'
  else
    FDescription := ADescription;
  fStatus := 1; // Draft
end;

end.
