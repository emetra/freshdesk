unit Freshdesk.Api;

interface

uses
  Freshdesk.Article,
  Rest.Client, Rest.Authenticator.Basic, Rest.Types,
  System.Classes, System.SysUtils;

type
  TFreshdeskApi = class( TComponent )
  strict private
    fClient: TRestClient;
    fAuth: THttpBasicAuthenticator;
  public
    procedure AfterConstruction; override;
    function PostArticle( const AFolder: Int64; const ATitle, ABody: string ): boolean;
  end;

implementation

uses
  System.Math;

{ TFreshdeskApi }

procedure TFreshdeskApi.AfterConstruction;
begin
  inherited;
  fAuth := THttpBasicAuthenticator.Create( Self );
  with fAuth do
  begin
    Username := 'QZkOTxT6dj8pC35yZ2o';
    Password := 'X';
  end;
  fClient := TRestClient.Create( Self );
  with fClient do
  begin
    Authenticator := fAuth;
    Accept := 'application/json';
    AcceptCharset := 'utf-8, *;q=0.8';
    BaseURL := 'https://ulriken.freshdesk.com/api/v2';
  end;
end;

function TFreshdeskApi.PostArticle( const AFolder: Int64; const ATitle, ABody: string ): boolean;
var
  fa: TFreshdeskArticlePost;
  r: TRestRequest;
begin
  fa := TFreshdeskArticlePost.Create( ATitle, ABody );
  r := TRestRequest.Create( nil );
  r.Client := fClient;
  r.Resource := 'solutions/folders/{id}/articles';
  try
    r.AddParameter( 'id', IntToStr( AFolder ), pkUrlSegment );
    r.AddBody<TFreshdeskArticlePost>( fa );
    r.Method := rmPost;
    r.Execute;
    Result := InRange( r.Response.StatusCode, 200, 201 );
  finally
    r.Free;
    fa.Free;
  end;
end;

end.
