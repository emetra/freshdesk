unit Html5Template;

interface

uses
  System.Classes, System.SysUtils;

type
  THtml5File = class( TStringList )
  strict private
    fEncoding: TUTF8Encoding;
  public
    constructor Create( const ATitle: string; ABody: string; const AIncludeBoostrap: boolean ); reintroduce;
    procedure BeforeDestruction; override;
    procedure SaveToFile( const AFileName: string ); reintroduce;
  end;

implementation

const
  PLACEHOLDER_BODY      = '{body}';
  PLACEHOLDER_BOOTSTRAP = '{bootstrap}';
  PLACEHOLDER_TITLE     = '{title}';

  HTML5_TEMPLATE = '<!doctype html>' +
  { } '<html lang="en">' +
  { } '<head>' +
  { } '  <meta charset="iso-8859-1">' +
  { } '  <meta name="viewport" content="width=device-width, initial-scale=1">' +
  { } '  <title>' + PLACEHOLDER_TITLE + '</title>' +
  { } '  <meta name="description" content="A simple HTML5 Template for new projects.">' +
  { } '  <meta name="author" content="SitePoint">' +
  { } '  <meta property="og:title" content="' + PLACEHOLDER_TITLE + '">' +
  { } '  <meta property="og:type" content="website">' +
  { } '  <meta property="og:url" content="https://www.sitepoint.com/a-basic-html5-template/">' + // Reference
  { } '  <meta property="og:description" content="' + PLACEHOLDER_TITLE + '">' +
  { } '  <meta property="og:image" content="image.png">' +
  { } '  <link rel="icon" href="/favicon.ico">' +
  { } '  <link rel="icon" href="/favicon.svg" type="image/svg+xml">' +
  { } '  <link rel="apple-touch-icon" href="/apple-touch-icon.png">' +
  // '  <link rel="stylesheet" href="css/styles.css?v=1.0">' +
  { } PLACEHOLDER_BOOTSTRAP +
  // We added this
  { } '</head>' +
  { } '<body class="container-fluid">' +
  /// <!-- your content here... -->
  { } '{body}' +
  // <!-- your scripts here... -->
  // {
  // '  <script src="js/scripts.js"></script>'+
  { } '</body>' +
  { } '</html>';

  HEAD_BOOTSTRAP =
  { } '<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">'#10 +
  { } '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>'#10;

  { THtml5File }

constructor THtml5File.Create( const ATitle: string; ABody: string; const AIncludeBoostrap: boolean );
var
  modifiableTemplate: string;

begin
  inherited Create;
  fEncoding := TUTF8Encoding.Create;
  modifiableTemplate := HTML5_TEMPLATE;
  modifiableTemplate := StringReplace( modifiableTemplate, PLACEHOLDER_TITLE, ATitle, [rfReplaceAll] );
  modifiableTemplate := StringReplace( modifiableTemplate, PLACEHOLDER_BODY, ABody, [] );
  if AIncludeBoostrap then
    modifiableTemplate := StringReplace( modifiableTemplate, PLACEHOLDER_BOOTSTRAP, HEAD_BOOTSTRAP, [] )
  else
    modifiableTemplate := StringReplace( modifiableTemplate, PLACEHOLDER_BOOTSTRAP, EmptyStr, [] );
  Text := modifiableTemplate;
end;

procedure THtml5File.BeforeDestruction;
begin
  fEncoding.Free;
  inherited;
end;

procedure THtml5File.SaveToFile( const AFileName: string );
begin
  inherited SaveToFile( AFileName, TUTF8Encoding.Create );
end;

end.
