unit HelpAndManual.Keywords;

interface

uses
  Xml.XmlIntf;

type
  TKeyword = record
    Translate: boolean;
    Keyword: string;
    constructor Create( ANode: IXmlNode );
  end;


implementation

{ TKeyword record }

constructor TKeyword.Create( ANode: IXmlNode );
begin
  Keyword := ANode.Text;
end;

end.
