unit HelpAndManual;

interface

uses
  Emetra.Logging.Interfaces,
  Generics.Collections,
  System.Classes,
  XmlIntf;

type

  TAttributes = class( TDictionary<string, string> )
    function AsString: string;
  end;

  THmElement = class( TObject )
  strict private
    fNode: IXmlNode;
    fHtmlTag: string;
    fAttributes: TAttributes;
  private
    fText: string;
  public
    constructor Create( const AHtmlTag: string; ANode: IXmlNode ); reintroduce;
    destructor Destroy; override;
    function AsHtml: string; dynamic;
    property Attributes: TAttributes read fAttributes;
    property HtmlTag: string read fHtmlTag;
    property Text: string read fText;
  end;

  THmBreak = class( THmElement )
  end;

  THmTitle = class( THmElement )
    function AsHtml: string; override;
  end;

  THmImage = class( THmElement )
    function AsHtml: string; override;
  end;

  THmLink = class( THmElement )
  end;

  THmCaption = class( THmElement )
  end;

  THmStyledElement = class( THmElement )
  private
    fStyleClass: string;
    fStyle: string;
  protected
    function StyleClass: string;
  public
    constructor Create( const AHtmlTag: string; ANode: IXmlNode ); reintroduce;
    function AsHtml: string; override;
  end;

  THmText = class( THmStyledElement )
  public
    constructor Create( AHtmlTag: string; ANode: IXmlNode ); reintroduce;
    function AsHtml: string; override;
  end;

  THmContainerElement = class( THmStyledElement )
  private
    fChildren: TObjectList<THmElement>;
    procedure ParseChildren( ANode: IXmlNode );
  public
    { Initialization }
    constructor Create( const AHtmlTag: string; ANode: IXmlNode );
    procedure BeforeDestruction; override;
    { Other methods }
    function AsHtml: string; override;
    function TryFind( const AClassType: TClass; out AElement: THmElement ): boolean;
  end;

  THmHeader = THmContainerElement;

  THmParagraphElement = class( THmContainerElement )
    function AsHtml: string; override;
  end;

  THmListItem = THmContainerElement;
  THmList = THmContainerElement;

  THmBody = class( THmContainerElement )
    function AsHtml: string; override;
  end;

  THmTopic = class( THmContainerElement )
  strict private
    function Get_Title: string;
    function Get_Body: string;
  public
    property Title: string read Get_Title;
    property Body: string read Get_Body;
  end;

implementation

uses
  System.RegularExpressions,
  Xml.XmlDoc,
  System.SysUtils, System.StrUtils, System.Variants;

function UseThisAttributeInHtml( const AKey: string ): boolean;
begin
  Result := true; // := ( A.Key = 'style' ) or ( A.Key = 'src' );
end;

{$REGION 'THmElement' }

constructor THmElement.Create( const AHtmlTag: string; ANode: IXmlNode );
var
  attrNo: integer;
begin
  inherited Create;
  if ANode.IsTextElement then
    fText := ANode.Text;
  fAttributes := TAttributes.Create;
  fNode := ANode;
  fHtmlTag := AHtmlTag;
  attrNo := 0;
  while attrNo < ANode.AttributeNodes.Count do
  begin
    if not VarIsNull( ANode.AttributeNodes[attrNo].NodeValue ) then
      fAttributes.Add( ANode.AttributeNodes[attrNo].NodeName, ANode.AttributeNodes[attrNo].NodeValue );
    inc( attrNo );
  end;
  fAttributes.Remove( 'translate' );
end;

destructor THmElement.Destroy;
begin
  fAttributes.Free;
  inherited;
end;

function THmElement.AsHtml: string;
begin
  if Trim( fText ) = EmptyStr then
    Result := Format( '<%s/>', [fHtmlTag] )
  else
    Result := Format( '<%0:s>%1:s</%0:s>', [fHtmlTag, fText] );
end;

{$ENDREGION}
{ THmText }

function THmText.AsHtml: string;
var
  headerTag: string;
begin
  if Attributes.TryGetValue( 'styleclass', headerTag ) and TRegEx.IsMatch( headerTag, '^Heading\d$' ) then
  begin
    { Replace styleclass tag with h1/h2 etc. tags }
    headerTag := StringReplace( headerTag, 'Heading', 'h', [rfReplaceAll, rfIgnoreCase] );
    Result := Format( '<%s>%s</%s>'#10, [headerTag, fText, headerTag] );
    { Exclude headers at level 1 }
    if headerTag = 'h1' then
      Result := EmptyStr;
  end
  { Code Examples }
  else if Attributes.TryGetValue( 'styleclass', headerTag ) and ( headerTag = 'Code Example' ) then
    Result := Format( '<span style="font-family: Courier New, Courier, monospace; color: #333399; font-weight: bold;">%s</span>', [fText] )
    { Normal text is not wrapped at all }
  else if Attributes.TryGetValue( 'styleclass', headerTag ) and ( headerTag = 'Normal' ) and ( Attributes.Count = 1 ) then
    Result := fText
  else
    Result := inherited AsHtml;
end;

constructor THmText.Create( AHtmlTag: string; ANode: IXmlNode );
begin
  inherited Create( AHtmlTag, ANode );
  fText := ANode.Text;
end;

{$REGION 'THmContainerElement' }

constructor THmContainerElement.Create( const AHtmlTag: string; ANode: IXmlNode );
begin
  inherited Create( AHtmlTag, ANode );
  fChildren := TObjectList<THmElement>.Create;
  ParseChildren( ANode );
end;

procedure THmContainerElement.BeforeDestruction;
begin
  fChildren.Free;
  inherited;
end;

function THmContainerElement.AsHtml: string;
var
  e: THmElement;
begin
  Result := EmptyStr;
  if HtmlTag <> EmptyStr then
    Result := Format( '<%s>', [HtmlTag] ) + Trim( fText );
  { Add content }
  for e in fChildren do
    Result := Result + e.AsHtml;
  { Close the tag, unless it is a div tag with no attributes. }
  if HtmlTag <> EmptyStr then
    Result := Result + Format( '</%s>', [HtmlTag] );
end;

procedure THmContainerElement.ParseChildren( ANode: IXmlNode );
var
  c: IXmlNode;
  i: integer;
begin
  i := 0;
  while i < ANode.ChildNodes.Count do
  begin
    c := ANode.ChildNodes[i];
    if ( c.NodeName = '#text' ) or ( c.NodeName = '#cdata-section' ) then
      fText := c.Text
    else if c.NodeName = 'text' then
      fChildren.Add( THmText.Create( 'span', c ) )
    else if c.NodeName = 'para' then
      fChildren.Add( THmParagraphElement.Create( 'div', c ) )
    else if c.NodeName = 'list' then
      fChildren.Add( THmList.Create( 'ul', c ) )
    else if c.NodeName = 'li' then
      fChildren.Add( THmListItem.Create( 'li', c ) )
    else if c.NodeName = 'br' then
      fChildren.Add( THmBreak.Create( 'br', c ) )
    else if c.NodeName = 'topic' then
      fChildren.Add( THmTopic.Create( EmptyStr, c ) )
    else if c.NodeName = 'link' then
      fChildren.Add( THmLink.Create( 'a', c ) )
    else if c.NodeName = 'title' then
      fChildren.Add( THmTitle.Create( 'title', c ) )
    else if c.NodeName = 'body' then
      fChildren.Add( THmBody.Create( 'body', c ) )
    else if c.NodeName = 'header' then
      fChildren.Add( THmHeader.Create( 'div', c ) )
    else if c.NodeName = 'image' then
      fChildren.Add( THmImage.Create( 'img', c ) )
    else if c.NodeName = 'caption' then
      fChildren.Add( THmCaption.Create( 'span', c ) )
    else if c.NodeName = 'table' then
      fChildren.Add( THmContainerElement.Create( 'table', c ) )
    else if c.NodeName = 'tr' then
      fChildren.Add( THmContainerElement.Create( 'tr', c ) )
    else if c.NodeName = 'td' then
      fChildren.Add( THmContainerElement.Create( 'td', c ) )
    else if ( c.NodeName = 'tab' ) then
      // Ignore
    else if ( c.NodeName = 'keywords' ) then
      // Ignore
    else
      raise Exception.CreateFmt( 'Unknown node: %s', [c.NodeName] );
    inc( i );
  end;
end;

function THmContainerElement.TryFind( const AClassType: TClass; out AElement: THmElement ): boolean;
var
  e: THmElement;
begin
  AElement := nil;
  for e in fChildren do
    if e.ClassType = AClassType then
    begin
      AElement := e;
      break;
    end;
  Result := Assigned( AElement );
end;

{$ENDREGION}
{ TAttributes }

function TAttributes.AsString: string;
var
  key, value: string;
begin
  Result := EmptyStr;
  if Self.Count > 0 then
    for key in Self.Keys do
    begin
      if UseThisAttributeInHtml( key ) and TryGetValue( key, value ) then
        Result := Result + Format( ' %s="%s" ', [key, value] );
    end;
end;

{ THmStyledElement }

constructor THmStyledElement.Create( const AHtmlTag: string; ANode: IXmlNode );
begin
  inherited Create( AHtmlTag, ANode );
  { Replace spaces in styleclasses, not allowed in html }
  if ANode.HasAttribute( 'styleclass' ) then
    fStyleClass := StringReplace( ANode.Attributes['styleclass'], ' ', EmptyStr, [rfReplaceAll] )
  else
    fStyleClass := EmptyStr;
  if ANode.HasAttribute( 'style' ) then
    fStyle := Format( ' style="%s"', [ANode.Attributes['style']] )
  else
    fStyle := EmptyStr;
end;

function THmStyledElement.StyleClass: string;
var
  localStyleClass: string;
begin
  if ( fStyleClass <> EmptyStr ) and ( fStyleClass <> 'Normal' ) then
  begin
    { Do replacements for headings }
    localStyleClass := TRegEx.Replace( fStyleClass, 'Heading', 'h' );
    Result := Format( ' class="%s"', [localStyleClass] )
  end
  else
    Result := EmptyStr;
end;

function THmStyledElement.AsHtml: string;
begin
  if Text = EmptyStr then
    Result := EmptyStr
  else
    Result := Format( '<%0:s%1:s%3:s>%2:s</%0:s>', [HtmlTag, StyleClass, Text, fStyle] ); // Todo.  Wait with conversion until here
end;

{ THmImage }

function THmImage.AsHtml: string;
var
  key: string;
  value: string;
begin
  Result := '<img ';
  for key in Attributes.Keys do
  begin
    if Attributes.TryGetValue( key, value ) then
      if ( key = 'src' ) then
        Result := Result + Format( ' %s="https://binaries.dips.no/Bilder/%s"', [key, value] )
      else
        Result := Result + Format( ' %s="%s"', [key, value] );
  end;
  Result := Result + '/>';
end;

{ THmTopic }

function THmTopic.Get_Body: string;
begin
  { TODO 1 -omrk -cBug : This includes the title, and we need to exclude that. }
  Result := Self.AsHtml;
end;

function THmTopic.Get_Title: string;
var
  e: THmElement;
begin
  if TryFind( THmTitle, e ) then
    Result := THmTitle( e ).Text
  else
    Result := '(untitled)';
end;

{ THmTitle }

function THmTitle.AsHtml: string;
begin
  Result := EmptyStr;
end;

{ THmParagraphElement }

function THmParagraphElement.AsHtml: string;
var
  e: THmElement;
  useTag: boolean;
  attrValue: string;
begin
  Result := EmptyStr;
  useTag := ( HtmlTag <> EmptyStr );
  if Attributes.Count = 0 then
    useTag := false
  else if Attributes.TryGetValue( 'styleclass', attrValue ) and ( attrValue = 'Normal' ) then
    useTag := false
  else
    GlobalLog.SilentWarning( '<div %s>', [Attributes.AsString] );
  // if ( fChildren.Count = 1 ) and ( fChildren[0].HtmlTag = 'span' ) then useTag := false;
  { Open the tag, unless it is a div tag with no attributes }
  if useTag then
    Result := Format( '<%s>', [HtmlTag] ) + #10;
  if HtmlTag <> EmptyStr then
    Result := Result + Trim( fText );
  { Add content }
  for e in fChildren do
    Result := Result + e.AsHtml;
  { Close the tag, unless it is a div tag with no attributes. }
  if useTag then
    Result := Result + Format( '</%s>', [HtmlTag] );
end;

{ THmBody }

function THmBody.AsHtml: string;
var
  e: THmElement;
begin
  Result := EmptyStr;
  for e in fChildren do
    Result := Result + e.AsHtml;
end;

end.
